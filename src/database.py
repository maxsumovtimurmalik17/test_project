import os

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker, declarative_base

if os.environ.get("RUNNING_ON_DOCKER_RUNNING"):
    DATABASE_URL = "postgresql+asyncpg://testuser:12345@db:5432/test"
else:
    DATABASE_URL = "postgresql+asyncpg://testuser:12345@localhost:5432/test"

engine = create_async_engine(DATABASE_URL, echo=True)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
session = async_session()
Base = declarative_base()
