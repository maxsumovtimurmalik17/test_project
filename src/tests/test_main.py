import pytest


@pytest.mark.asyncio
async def test_get_clients(client):
    URL = "/api/clients/"
    response = client.get(URL)  # Отправить GET-запрос по указанному URL
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert response.json() == []  # Проверить, что JSON-ответ является пустым списком


@pytest.mark.asyncio
async def test_add_client(client):
    test_data = {
        "id": 1,
        "phone": "+798989898",
        "code": 123,
        "tag": "example",
        "timezone": "UTC",
    }
    URL = "/api/clients/"
    response = await client.post(
        URL, json=test_data
    )  # Отправить POST-запрос по указанному URL с JSON-данными
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert (
        response.json() == test_data
    )  # Проверить, что JSON-ответ соответствует тестовым данным


@pytest.mark.asyncio
async def test_put_client(client):
    test_data = {
        "id": 1,
        "phone": "+000000",
        "code": 111,
        "tag": "new_example",
        "timezone": "UTC+1",
    }
    URL = f'/api/clients/{test_data.get("id")}/'
    response = await client.put(
        URL, json=test_data
    )  # Отправить PUT-запрос по указанному URL с JSON-данными
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert (
        response.json() == test_data
    )  # Проверить, что JSON-ответ соответствует тестовым данным


@pytest.mark.asyncio
async def test_delete_client(client):
    URL = f"/api/clients/{1}/"
    response = await client.delete(URL)  # Отправить DELETE-запрос по указанному URL
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert response.json() == {
        "detail": "Deleted successfully"
    }  # Проверить, что JSON-ответ соответствует ожидаемому сообщению


@pytest.mark.asyncio
async def test_get_distribution(client):
    URL = "/api/distribution/"
    response = await client.get(URL)  # Отправить GET-запрос по указанному URL
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert response.json() == []  # Проверить, что JSON-ответ является пустым списком


@pytest.mark.asyncio
async def test_add_distribution(client):
    test_data = {
        "id": 2,
        "start_datetime": "2023-05-11T21:25:30",
        "text": "Sample text",
        "filter": {"tag": "example", "code": 123},
        "end_datetime": "2023-06-13T22:30:00",
    }
    URL = "/api/distribution/"
    response = await client.post(URL, json=test_data)
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert (
        response.json() == test_data
    )  # Проверить, что JSON-ответ соответствует тестовым данным


@pytest.mark.asyncio
async def test_put_distribution(client):
    test_data = {
        "id": 2,
        "start_datetime": "2023-05-11T21:25:30",
        "text": "New text",
        "filter": {"tag": "example", "code": 111},
        "end_datetime": "2023-06-13T22:30:00",
    }
    URL = f'/api/distribution/{test_data.get("id")}/'
    response = await client.put(URL, json=test_data)
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert (
        response.json() == test_data
    )  # Проверить, что JSON-ответ соответствует тестовым данным


@pytest.mark.asyncio
async def test_delete_distribution(client):
    URL = f"/api/distribution/{2}/"
    response = await client.delete(URL)
    assert response.status_code == 200  # Проверить, что код ответа равен 200 (OK)
    assert response.json() == {
        "detail": "Deleted successfully"
    }  # Проверить, что JSON-ответ соответствует ожидаемому сообщению
