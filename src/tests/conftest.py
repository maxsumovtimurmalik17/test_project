import pytest
from fastapi.testclient import TestClient
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from src.database import session, Base
from src.main import app


@pytest.fixture(scope="session")
def client():
    SQLALCHEMY_DATABASE_URL = "sqlite+aiosqlite:///./tests./test.db"

    engine = create_async_engine(SQLALCHEMY_DATABASE_URL)
    test_session = sessionmaker(bind=engine, class_=AsyncSession)

    Base.metadata.create_all(bind=engine)

    def override_get_db():
        try:
            db = test_session()
            yield db
        finally:
            db.close()

    app.dependency_overrides[session] = override_get_db
    test_client = TestClient(app)
    yield test_client

    app.dependency_overrides.pop(session)
