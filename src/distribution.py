import logging
import logging.config
import datetime
import json

import aiohttp
import asyncio

from sqlalchemy import select, and_

from src.models import Distribution, Client, Message
from src.log_config import dict_config

# Конфигурация логирования
logging.config.dictConfig(dict_config)
logger = logging.getLogger("proj log")

OPENAPI_URL = "https://probe.fbrq.cloud/v1/send/{message_id}"
JWT_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUyNzI4NTEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS90aW11cm1hbGlrMTcifQ.nVd06wFM1DXDST8TdpYCZ1zZU5lD64Q_yYhT_KYNC60"
HEADERS = {"Authorization": JWT_TOKEN, "Content-Type": "application/json"}


async def async_post(url, data, headers):
    """
    Асинхронная функция для выполнения POST-запроса
    """
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data, headers=headers) as response:
            return response


async def process_distribution(distribution: Distribution, session):
    """
    Обработка распределения сообщений
    """
    current = datetime.datetime.now()
    if distribution.start_datetime < current < distribution.end_datetime:
        await start_distribution(distribution, session)
    elif current < distribution.start_datetime:
        current_delta = distribution.start_datetime - current
        seconds_delta = current_delta.total_seconds()
        logger.debug(f"Task delayed for {seconds_delta}s")
        await asyncio.sleep(seconds_delta)
        await start_distribution(distribution, session)


async def start_distribution(distribution: Distribution, session):
    """
    Запуск распределения сообщений
    """
    filter = distribution.filter
    query = select(Client).where(
        and_(Client.code == filter["code"], Client.tag == filter["tag"])
    )
    async with session.begin():
        result = await session.execute(query)
    clients = result.scalars().all()
    for client in clients:
        new_message = Message(distribution_id=distribution.id, client_id=client.id)
        async with session.begin():
            session.add(new_message)
        response = {
            "id": new_message.id,
            "phone": client.phone,
            "text": distribution.text,
        }
        json_data = json.dumps(response)
        res = await async_post(
            url=OPENAPI_URL.format(message_id=new_message.id),
            data=json_data,
            headers=HEADERS,
        )
        logger.debug(
            f"Message id({new_message.id} was sent to Client id({client.id}) by Distribution id({distribution.id}). Response: {res.status}"
        )
