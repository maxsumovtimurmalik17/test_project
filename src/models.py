import datetime

from sqlalchemy import Column, String, Integer, DateTime, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB

from src.database import Base


class Distribution(Base):
    __tablename__ = "Distribution"

    id = Column(Integer, primary_key=True, index=True)
    start_datetime = Column(DateTime, index=True)
    text = Column(String, index=True)
    filter = Column(JSONB, index=True)
    end_datetime = Column(DateTime, index=True)


class Client(Base):
    __tablename__ = "Client"

    id = Column(Integer, primary_key=True, index=True)
    phone = Column(String, index=True)
    code = Column(Integer, index=True)
    tag = Column(String, index=True)
    timezone = Column(String, index=True)


class Message(Base):
    __tablename__ = "Message"

    id = Column(Integer, primary_key=True, index=True)
    creation_datetime = Column(DateTime, default=datetime.datetime.now(), index=True)
    status = Column(String, default="distributed", index=True)
    distribution_id = Column(Integer, ForeignKey("Distribution.id", ondelete="CASCADE"))
    client_id = Column(Integer, ForeignKey("Client.id", ondelete="CASCADE"))
