import datetime

from pydantic import BaseModel


class ClientSchema(BaseModel):
    id: int
    phone: str
    code: int
    tag: str
    timezone: str

    class Config:
        orm_mode = True


class DistributionSchema(BaseModel):
    id: int
    start_datetime: datetime.datetime
    text: str
    filter: dict
    end_datetime: datetime.datetime

    class Config:
        orm_mode = True


class MessageSchema(BaseModel):
    id: int
    creation_datetime: datetime.datetime
    status: str
    distribution_id: int
    client_id: int

    class Config:
        orm_mode = True
