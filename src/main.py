import logging
import logging.config
from typing import List, Dict

from fastapi import FastAPI, HTTPException, APIRouter
from sqlalchemy import select, func, and_
from prometheus_fastapi_instrumentator import Instrumentator

import src.models
from src.log_config import dict_config
from src.models import Client, Distribution, Message
from src.database import engine, session
from src.schemas import ClientSchema, DistributionSchema
from src.distribution import process_distribution

# Создаем экземпляр FastAPI
app = FastAPI()

# Создаем роутеры для клиентов и распределения
client_router = APIRouter(tags=["Client"])
dist_router = APIRouter(tags=["Distribution"])

# Создаем экземпляр Instrumentator для инструментирования FastAPI
instrumentator = Instrumentator().instrument(app)

# Конфигурируем логирование
logging.config.dictConfig(dict_config)
logger = logging.getLogger("proj log")


@app.on_event("startup")
async def startup():
    # Регистрируем инструментатор при старте приложения
    instrumentator.expose(app)
    # Создаем соединение с базой данных и выполняем миграции
    async with engine.begin() as conn:
        await conn.run_sync(src.models.Base.metadata.create_all)
    logger.debug(f"Web app started")


@app.on_event("shutdown")
async def shutdown():
    # Закрываем сессию и освобождаем ресурсы при остановке приложения
    await session.close()
    await engine.dispose()
    logger.debug(f"Web app stopped")


@app.get("/")
async def test():
    """
    Test Endpoint
    """
    return "Hello"


@client_router.get("/api/clients/")
async def get_clients() -> List[ClientSchema]:
    """
    Get all clients
    :return:
    """
    async with session.begin():
        result = await session.execute(select(Client))
    clients = result.scalars().all()
    logger.debug(f"Get all clients")
    return clients


@client_router.post("/api/clients/")
async def add_client(client: ClientSchema) -> ClientSchema:
    """
    Add new client
    """
    new_client = Client(**client.dict())
    async with session.begin():
        session.add(new_client)
    logger.debug(f"Add client id({new_client.id})")
    return new_client


@client_router.put("/api/clients/{client_id}")
async def update_client(client_id: int, client: ClientSchema) -> ClientSchema:
    """
    Update client info
    """
    async with session.begin():
        existing_client = await session.get(Client, client_id)
        if not existing_client:
            raise HTTPException(status_code=404, detail="Client not found")

        for field, value in client.dict(exclude_unset=True).items():
            setattr(existing_client, field, value)

            session.add(existing_client)
    logger.debug(f"Update client id({client_id})")
    return existing_client


@client_router.delete("/api/clients/{client_id}")
async def delete_client(client_id: int) -> dict:
    """
    Delete client
    """
    async with session.begin():
        existing_client = await session.get(Client, client_id)
        if not existing_client:
            raise HTTPException(status_code=404, detail="Client not found")

        await session.delete(existing_client)
    logger.debug(f"Delete client id({client_id})")
    return {"detail": "Deleted successfully"}


@dist_router.get("/api/distribution/")
async def get_dist() -> List[DistributionSchema]:
    """
    Get all Distributions
    :return:
    """
    async with session.begin():
        result = await session.execute(select(Distribution))
    dist = result.scalars().all()
    logger.debug(f"Get all Distributions")
    return dist


@dist_router.post("/api/distribution/")
async def add_dist(dist: DistributionSchema) -> DistributionSchema:
    """
    Add new Distribution
    """
    new_dist = Distribution(**dist.dict())
    async with session.begin():
        session.add(new_dist)
    await process_distribution(new_dist, session)
    logger.debug(f"Add distribution id({new_dist.id})")
    return new_dist


@dist_router.put("/api/distribution/{dist_id}")
async def update_dist(dist_id: int, client: ClientSchema) -> ClientSchema:
    """
    Update Distribution info
    """
    async with session.begin():
        existing_dist = await session.get(Distribution, dist_id)
        if not existing_dist:
            raise HTTPException(status_code=404, detail="Client not found")

        for field, value in client.dict(exclude_unset=True).items():
            setattr(existing_dist, field, value)

            session.add(existing_dist)
    logger.debug(f"Uodate distribution id({dist_id})")
    return existing_dist


@dist_router.delete("/api/distribution/{dist_id}")
async def delete_dist(dist_id: int) -> dict:
    """
    Delete Distribution
    """
    async with session.begin():
        existing_dist = await session.get(Distribution, dist_id)
        if not existing_dist:
            raise HTTPException(status_code=404, detail="Distribution not found")

        await session.delete(existing_dist)
    logger.debug(f"Delete distribution id({dist_id})")
    return {"detail": "Deleted successfully"}


@dist_router.get("/api/distribution/stats/")
async def get_all_dist_stats() -> Dict[int, Dict[str, int]]:
    """
    Get all Distributions statistics
    """
    async with session.begin():
        result = await session.execute(select(Distribution))
        dists = result.scalars().all()
        result = await session.execute(select(Message))
        status_types = set([a.status for a in result.scalars().all()])
        response = dict()
        for dist in dists:
            response[dist.id] = {}
            for status in status_types:
                query = (
                    select(func.count())
                    .select_from(Message)
                    .where(
                        and_(
                            Message.distribution_id == dist.id, Message.status == status
                        )
                    )
                )
                result = await session.execute(query)
                num_of_msg = result.scalars().one()
                response[dist.id][status] = num_of_msg
    logger.debug(f"Get all Distributions statistics")
    return response


@dist_router.get("/api/distribution/stats/{dist_id}")
async def get_all_dist_stats(dist_id: int) -> Dict[str, int]:
    """
    Get Distribution statistics
    """
    async with session.begin():
        response = dict()
        result = await session.execute(select(Message))
        status_types = set([a.status for a in result.scalars().all()])
        for status in status_types:
            query = (
                select(func.count())
                .select_from(Message)
                .where(
                    and_(Message.distribution_id == dist_id, Message.status == status)
                )
            )
            result = await session.execute(query)
            num_of_msg = result.scalars().one()
            response[status] = num_of_msg
    logger.debug(f"Get Distribution id({dist_id}) statistics")
    return response


app.include_router(client_router)
app.include_router(dist_router)
